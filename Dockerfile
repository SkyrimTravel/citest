FROM python:latest
WORKDIR /application
COPY ./requirements.txt .
RUN pip install -r requirements.txt
COPY app/tests/test_main.py ./main.py
EXPOSE 8000
CMD uvicorn main:app --reload
