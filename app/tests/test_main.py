"""Test"""
from fastapi.testclient import TestClient
from fastapi import FastAPI, HTTPException
from pydantic import BaseModel

fake_db = {
    "foo": {"id": "foo", "title": "Foo"},
    "bar": {"id": "bar", "title": "Bar"},
}

app = FastAPI()


class Course(BaseModel):
    """Course"""
    id: str
    title: str


@app.get("/courses/{course_id}", response_model=Course)
async def read_main(course_id: str):
    """read_main"""
    if course_id in fake_db:
        return fake_db[course_id]
    raise HTTPException(status_code=404, detail="Course does not exist")


@app.post("/courses/", response_model=Course)
async def create_course(course: Course):
    """create_course"""
    if course.id in fake_db:
        raise HTTPException(status_code=400, detail="Course already exists")
    fake_db[course.id] = course
    return course


client = TestClient(app)


def test_read_course():
    """test_read_course"""
    response = client.get("/courses/foo")
    assert response.status_code == 200
    assert response.json() == {
        "id": "foo",
        "title": "Foo",
    }


def test_read_inexistent_course():
    """test_read_inexistent_course"""
    response = client.get("/courses/baz")
    assert response.status_code == 404
    assert response.json() == {"detail": "Course does not exist"}


def test_create_course():
    """test_create_course"""
    response = client.post(
        "/courses/",
        json={"id": "foobar", "title": "Foo Bar"},
    )
    assert response.status_code == 200
    assert response.json() == {
        "id": "foobar",
        "title": "Foo Bar"
    }


def test_create_existing_course():
    """test_create_existing_course"""
    response = client.post(
        "/courses/",
        json={
            "id": "foo",
            "title": "The Foo ID Stealers"
        },
    )
    assert response.status_code == 400
    assert response.json() == {"detail": "Course already exists"}
